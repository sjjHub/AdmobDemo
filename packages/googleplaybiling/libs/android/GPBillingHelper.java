/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated engine source code (the "Software"), a limited,
 worldwide, royalty-free, non-assignable, revocable and non-exclusive license
 to use Cocos Creator solely to develop games on your target platforms. You shall
 not use Cocos Creator software for developing other software or tools that's
 used for developing games. You are not granted to publish, distribute,
 sublicense, and/or sell copies of Cocos Creator.

 The software or tools in this License Agreement are licensed, not sold.
 Xiamen Yaji Software Co., Ltd. reserves all rights not expressly granted to you.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

package org.cocos2dx.javascript;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import java.lang.reflect.Array;
import java.util.ArrayList;


import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

public class AdmobHelper implements RewardedVideoAdListener {
    @Override
    public void onRewardedVideoAdLoaded() {
        AdmobHelper.getInstance().handlerMessageToGame("onAdLoaded",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        AdmobHelper.getInstance().handlerMessageToGame("onRewardedVideoAdOpened",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoStarted() {
        AdmobHelper.getInstance().handlerMessageToGame("onRewardedVideoStarted",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoAdClosed() {
        AdmobHelper.getInstance().handlerMessageToGame("onRewardedVideoAdClosed",this.mRewardedPlacementId);
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        AdmobHelper.getInstance().handlerMessageToGame("onRewarded",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        AdmobHelper.getInstance().handlerMessageToGame("onRewardedVideoAdLeftApplication",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        AdmobHelper.getInstance().handlerMessageToGame("onError",this.mRewardedPlacementId);
    }

    @Override
    public void onRewardedVideoCompleted() {
        AdmobHelper.getInstance().handlerMessageToGame("onRewardedVideoCompleted",this.mRewardedPlacementId);
    }

    class ADS_TYPE {
        public static final int BANNER = 1;
        public static final int REWARDEDVIDEO = 2;
        public static final int INTERSTITIAL = 3;
    }

    class BANNER_SIZE {
        public static final int BANNER = 1;
        public static final int FULL_BANNER = 2;
        public static final int SMART_BANNER = 3;
        public static final int LARGE_BANNER = 4;
    }

    class BANNER_POSITION {
        public static final int ALIGN_PARENT_TOP = 1;
        public static final int ALIGN_PARENT_BOTTOM = 2;
        public static final int CENTER_IN_PARENT = 3;

    }

    private static final String TAG = "Admob";
    private static final String APP_ID = "ca-app-pub-3940256099942544/6300978111";
    private static AdmobHelper mInstance;
    private ArrayList<AdView> adList;
    private ArrayList<InterstitialAd> adInterstitialList;
    private RewardedVideoAd mRewardedVideoAd;
    private String mRewardedPlacementId = "";
    AdmobHelper() {
        adList = new ArrayList<>();
        adInterstitialList = new ArrayList<>();
        Context ctx = SDKWrapper.getInstance().getContext();
        MobileAds.initialize(ctx,APP_ID);
    }

    public static AdmobHelper getInstance() {
        if (null == mInstance) {
            mInstance = new AdmobHelper();
        }
        return mInstance;
    }

    private AdView getAd(String placementId) {
        AdView ad = null;
        for (int i = 0; i < adList.size(); i++) {
            AdView item = adList.get(i);
            if (item.getAdUnitId().equals(placementId)) {
                ad = item;
                break;
            }
        }
        return ad;
    }

    private InterstitialAd getInterstitialAd(String placementId){
        InterstitialAd ad = null;
        for (int i = 0; i < adInterstitialList.size(); i++) {
            InterstitialAd item = adInterstitialList.get(i);
            if (item.getAdUnitId().equals(placementId)) {
                ad = item;
                break;
            }
        }
        return ad;
    }

    private AdView removeAd(String placementId) {
        AdView ad = null;
        for (int i = 0; i < adList.size(); i++) {
            AdView item = adList.get(i);
            if (item.getAdUnitId().equals(placementId)) {
                ad = item;
                adList.remove(item);
                break;
            }
        }
        return ad;
    }

    private InterstitialAd removeInterstitialAd(String placementId){
        InterstitialAd ad = null;
        for (int i = 0; i < adInterstitialList.size(); i++) {
            InterstitialAd item = adInterstitialList.get(i);
            if (item.getAdUnitId().equals(placementId)) {
                ad = item;
                adList.remove(item);
                break;
            }
        }
        return ad;
    }


    private void load(String placementId) {
        Log.d(TAG, "load ad " + placementId);
        AdView ad = this.getAd(placementId);
        if (ad != null) {
            AdRequest adRequest = new AdRequest.Builder().build();
            ad.loadAd(adRequest);
        } else {
            Log.w(TAG, "load ad fail" + placementId);
        }
    }

    private void loadInterstitial(String placementId){
        Log.d(TAG, "load Interstitial " + placementId);
        InterstitialAd ad = this.getInterstitialAd(placementId);
        if (ad != null) {
            AdRequest adRequest = new AdRequest.Builder().build();
            ad.loadAd(adRequest);
        } else {
            Log.w(TAG, "load ad fail" + placementId);
        }
    }

    private void loadRewarded(final String placementId){
        Log.d(TAG, "load Rewarded " + placementId);
        this.mRewardedPlacementId = placementId;
        this.mRewardedVideoAd.loadAd(placementId,new AdRequest.Builder().addTestDevice("AD03450E17D50EFB0BBA4D19DC0EC370").build());
    }

    private void showInterstitial(String placementId){
        InterstitialAd ad = this.getInterstitialAd(placementId);
        if(ad.isLoaded()){
            ad.show();
        }else{
            Log.w(TAG, "InterstitialAd needs to call show after ad onAdLoaded");
        }
    }

    private void showRewarded(final String placementId){
       if(this.mRewardedVideoAd.isLoaded()){
           this.mRewardedVideoAd.show();
       }else{
           Log.w(TAG, "RewardedAd needs to call show after ad onAdLoaded");
       }
    }

    private boolean _isExist(String placementId) {
        boolean exist = false;
        for (int i = 0; i < adList.size(); i++) {
            AdView item = adList.get(i);
            if (placementId.equals(item.getAdUnitId())) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    private boolean _isExistInterstitial(String placementId){
        boolean exist = false;
        for (int i = 0; i < adInterstitialList.size(); i++) {
            InterstitialAd item = adInterstitialList.get(i);
            if (placementId.equals(item.getAdUnitId())) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    private void destroy(String placementId) {
        AdView ad = this.removeAd(placementId);
        if (ad != null) {
            Log.d(TAG, "destroy----");
            ad.destroy();
        }
    }

    private void addBanner(final String placementId, int adSize, int position) {
        boolean exist = this._isExist(placementId);

        if (exist) {
            this.removeAd(placementId);
        }
        AdSize size;
        //parse banner size;
        switch (adSize) {
            case BANNER_SIZE.BANNER:
                size = AdSize.BANNER;
                break;
            case BANNER_SIZE.FULL_BANNER:
                size = AdSize.FULL_BANNER;
                break;
            case BANNER_SIZE.SMART_BANNER:
                size = AdSize.SMART_BANNER;
                break;
            case BANNER_SIZE.LARGE_BANNER:
                size = AdSize.LARGE_BANNER;
                break;
            default:
                size = AdSize.SMART_BANNER;
                break;
        }

        //parse banner position
        int pos;
        switch (position) {
            case BANNER_POSITION.ALIGN_PARENT_BOTTOM:
                pos = RelativeLayout.ALIGN_PARENT_BOTTOM;
                break;
            case BANNER_POSITION.CENTER_IN_PARENT:
                pos = RelativeLayout.CENTER_IN_PARENT;
                break;
            default:
                pos = RelativeLayout.ALIGN_PARENT_TOP;
                break;
        }

        Context ctx = SDKWrapper.getInstance().getContext();
        AdView ad = new AdView(ctx);
        ad.setAdSize(size);
        ad.setAdUnitId(placementId);
        Log.d(TAG, "ad size is " + adList.size());
        adList.add(ad);

        RelativeLayout oriLayout = new RelativeLayout(ctx);
        ((Cocos2dxActivity) ctx).addContentView(oriLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        RelativeLayout.LayoutParams adLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adLayoutParams.addRule(pos);
        oriLayout.addView(ad, adLayoutParams);

        ad.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                AdmobHelper.this.handlerMessageToGame("onError",placementId);
                Log.d(TAG, "onError---------banner" + errorCode);
            }

            @Override
            public void onAdLoaded() {
                AdmobHelper.this.handlerMessageToGame("onAdLoaded",placementId);
                Log.d(TAG, "onAdLoaded---------banner");
            }

            @Override
            public void onAdOpened() {
                AdmobHelper.this.handlerMessageToGame("onAdOpened",placementId);
                Log.d(TAG, "onAdOpened---------banner");
            }

            @Override
            public void onAdLeftApplication() {
                AdmobHelper.this.handlerMessageToGame("onAdLeftApplication",placementId);
                Log.d(TAG, "onAdLeftApplication---------banner");
            }
        });

        Log.d(TAG, "create Banner");
        this.handlerMessageToGame("onCreated", ad.getAdUnitId());
    }

    private void addInterstitalAd(final String placementId){
        boolean exist = this._isExistInterstitial(placementId);

        if (exist) {
            this.removeInterstitialAd(placementId);
        }
        Context ctx = SDKWrapper.getInstance().getContext();
        InterstitialAd ad = new InterstitialAd(ctx);
        ad.setAdUnitId(placementId);
        adInterstitialList.add(ad);

        ad.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                AdmobHelper.getInstance().handlerMessageToGame("onAdLoaded",placementId);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                AdmobHelper.getInstance().handlerMessageToGame("onError",placementId,errorCode);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                AdmobHelper.getInstance().handlerMessageToGame("onAdOpened",placementId);
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                AdmobHelper.getInstance().handlerMessageToGame("onAdLeftApplication",placementId);
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                AdmobHelper.getInstance().handlerMessageToGame("onAdClosed",placementId);
            }
        });

        this.handlerMessageToGame("onCreated", ad.getAdUnitId());
        Log.d(TAG, "create Interstital---------");
    }

    private void addRewardedAd(String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        this.mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(ctx);
        this.mRewardedVideoAd.setRewardedVideoAdListener(this);
        this.handlerMessageToGame("onCreated", placementId);
    }


    private void handlerMessageToGame(final String eventName, final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString("cc.AdmobAds._eventReceiver('" + eventName + "','" + placementId + "')");
            }
        });
    }

    private void handlerMessageToGame(final String eventName, final String placementId, final int error) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnGLThread(new Runnable() {
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString("cc.AdmobAds._eventReceiver('" + eventName + "','" + placementId + "'," + error + ")");
            }
        });
    }

    public static void createAd(final String placementId, final int size, final int position) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdmobHelper.getInstance().addBanner(placementId, size, position);
            }
        });
    }

    public static void createAd(final int type, final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (type == ADS_TYPE.INTERSTITIAL) {
                    AdmobHelper.getInstance().addInterstitalAd(placementId);
                } else if (type == ADS_TYPE.REWARDEDVIDEO) {
                    AdmobHelper.getInstance().addRewardedAd(placementId);
                }
            }
        });

    }

    public static void loadAd(final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdmobHelper.getInstance().load(placementId);
            }
        });
    }

    public static void loadRewardedAd(final String placementId){
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdmobHelper.getInstance().loadRewarded(placementId);
            }
        });
    }


    public static void loadInterstitialAd(final String placementId){
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdmobHelper.getInstance().loadInterstitial(placementId);
            }
        });
    }

    public static void showInterstitialAd(final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "showInterstitialAd----");
                AdmobHelper.getInstance().showInterstitial(placementId);
            }
        });
    }

    public static void showRewardedAd(final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "showRewardedAd----");
                AdmobHelper.getInstance().showRewarded(placementId);
            }
        });
    }

    public static void destroyAd(final String placementId) {
        Context ctx = SDKWrapper.getInstance().getContext();
        ((Cocos2dxActivity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "destroyAd----");
                AdmobHelper.getInstance().destroy(placementId);
            }
        });
    }
}
